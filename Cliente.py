from xmlrpc.client import ServerProxy
import os
clear = lambda: os.system('cls')

s = ServerProxy('http://localhost:20064',allow_none=True) #esta variable s sera la que manejará todas las acciones que se llevan a cabo en el servidor mediante este cliente
#se utiliza el puerto declarado anteriormente en el servidor

s.set("Zapatos","4000") #se guarda una entrada en el diccionario del servidor para pruebas

def MENU():
    print("Tienda de Productos")
    print("Opciones")
    print("1. Mostrar Objetos")
    print("2. Setear Objetos")
    print("3. Obtener Objeto")
    print("4. Eliminar Objeto")
    print("Cualquier otra tecla para salir")

menu = True

while(menu == True):
    MENU()
    option = input("Escoja su opcion:  ")
    if option == "1":
        print(s.keys())
        input("Presione Cualquier tecla para continuar...")
    elif option == "2":
        nombre = input("Nombre del producto:  ")
        costo = input("Costo del producto:  ")
        s.set(nombre,costo)
        print("Producto Introducido")
        input("Presione Cualquier tecla para continuar...")
    elif option == "3":
        try:
            print(s.get(input("Introduzca el nombre del producto que desea obtener:  ")))
        except:
            print("No existe tal producto")
        input("Presione Cualquier tecla para continuar...")
    elif option == "4":
        try:
            s.delete(input("Introduzca el nombre del producto que desea eliminar:  "))
        except:
            print("No existe tal producto")
        input("Presione Cualquier tecla para continuar...")
    else:
        menu = False
    clear()
