from xmlrpc.server import SimpleXMLRPCServer

class RPC:
    metodos_rpc = ['get','set','delete','exists','keys'] #En este vector se guardan los metodos que se van a registrar en nuestro servidor mediante la funcion register_function()

    def __init__(self,direccion):
        self.productos = {} #se inicializa un diccionario para almacenar los datos de los productos
        self._servidor = SimpleXMLRPCServer(direccion, allow_none=True)

        for metodo in self.metodos_rpc:
            self._servidor.register_function(getattr(self,metodo))
    
    def get(self,nombre):
        return self.productos[nombre]

    def set(self,nombre,valor):
        self.productos[nombre] = valor

    def delete(self,nombre):
        del self.productos[nombre]

    def exists(self, nombre):
        return nombre in self.productos

    def keys(self):
        return list(self.productos)

    def iniciar_servidor(self):
        self._servidor.serve_forever()

if __name__  == '__main__':
    rpc = RPC(('',20064)) #Se crea un objeto de tipo RPC que recibe una direccion de puerto donde va a estar funcionando el servidor
    print('Se ha iniciado el servidor RPC')
    rpc.iniciar_servidor()